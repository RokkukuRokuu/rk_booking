import React from 'react';
import { StyleSheet,View,Text,TouchableOpacity,ScrollView} from 'react-native';
import {AntDesign} from '@expo/vector-icons';

function youndal(year){
  if(year%4==0 && year%100!=0 || year%400==0)
    return 1;
  else 
    return 0;
  
}

function totalDay(year,month){
  if(month==1 || month==3 || month==5 || month==7 || month==8 || month==10 || month==12)
    return 31;
  else if(month==4 || month==6 || month==9)
    return 30;
  else 
    return 28+youndal(year);
  
}

export function createDate(Year,weekly,nowDay){
  const startYear = Year.startYear;
  const endYear = Year.endYear;
  const nowYear = nowDay.year;
  const CALENDER = {};
  for(let year = (nowYear-startYear); year<= (nowYear+endYear);year++){
    
    const MONTH = {};
    for(let month = 0; month < 12; month++){
      
      var days = totalDay(year,month+1);
      var jucha = 1;
      const DAYS = {};
      for(let day=1;day<=days;day++){
       
        //과거 / 현재 / 미래 를 나눠야 한다
        var type = timeType(nowDay,year,month+1,day);
        var a = new Date(year,month,day);
        var d = a.getDay();
        var w = weekly[d];
        var key = String(jucha-1+'-'+d);
        
        const date = {'row':d,'column':jucha-1,'day':day,'week':jucha,'weekly':w,
        'type':type,'year':year,'month':month+1,'key':key};
        DAYS[key] = date;
        if(d==6) jucha +=1;
      }
      
      MONTH[String(month+1)]=DAYS;
    }
    
    CALENDER[String(year)]=MONTH;
  }
  return CALENDER;
}

export function timeType(nowDay,year,month,day){
    let dd = nowDay.day;
    let mm = nowDay.month;
    let yy = nowDay.year;

    const a= (year*365)+(month*30)+day;
    const b= (yy*365)+(mm*30)+dd;
    if(a == b) return 1;
    else if(a > b) return 2;
    else return 0;

}

export default class BK_Calendar extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            year:this.props.year,
            backgroundColor:this.props.backgroundColor,
            defaultColor:this.props.defaultColor,
            accentColor:this.props.accentColor,
            borderColor:this.props.borderColor,
            dateCallback:this.props.dateCallback,
            weekly:this.props.weekly,
            nowDay:this.props.nowDay,
            marker:this.props.marker,
            reRander:false,
            defaultText:this.props.defaultText,
            calendarType:this.props.calendarType,
            viewLevel:3,
        }
    }

    componentWillMount(){
        const {year,weekly,nowDay,marker} = this.state;
        const changeCalendar = {year:nowDay.year,month:nowDay.month};
        const standardDay = {year:nowDay.year,month:nowDay.month,day:nowDay.day};
        const calendar = createDate(year,weekly,nowDay);
        const cal = this.getAllDay(calendar,nowDay.year,nowDay.month);
        
        for(k in cal){
          if(cal[k].type == 1) {
            Object.assign(nowDay,{key:k});
            break;
          }
        }
        const week = calendar[nowDay.year][nowDay.month][nowDay.key].row;
        this.setState({
          calendar:calendar,
          nowDay:nowDay,
          changeCalendar: changeCalendar,
          standardDay: standardDay,
          week: week,
        });
    }

    getWeeklyStyle = (key) =>{
      const {week,defaultColor,borderColor} = this.state;
      if(week == key){
        return {
          fontSize:20,
          color:borderColor,
        }
      }else{
        return {
          fontSize:15,
          color:defaultColor,
        }
      }
    }

    getWeeklyView = (weekly) =>{
      let weekview = [];
      for(key in weekly){
        const k = key;
        weekview.push(<View key={key} style={styles.weekView}><Text style={this.getWeeklyStyle(key)}>{weekly[key]}</Text></View>)
      }
      return weekview;
    }

    getAllDay = (calendar,Year,Month) =>{
      if(calendar[Year] != undefined){
        if(calendar[Year][Month] != undefined){
          return calendar[Year][Month];
        }else console.log('[Calendar][MONTH]','(MONTH) The input format is incorrect.'); 
      }else console.log('[Calendar][YEAR]','(YEAR) The input format is incorrect.');
    }

    getColor = (type) =>{
      const {accentColor,defaultColor,calendarType} = this.state;
      if(type == 0){
        if(calendarType == 'Booking') return '#e2e2e2';
        else if(calendarType == 'Calendar')return defaultColor;
        else return defaultColor;
      }else if(type == 1){
        return accentColor;
      }else if(type == 2){ 
        return defaultColor;
      }
      return '#fff';
    }

    DayStyle = (type) =>{
      const {borderColor} = this.state;
      let style = {
        alignItems:'center',
        justifyContent:'flex-end',
        width:25,
      }; 
      if(type == 1){
        style = {
          alignItems:'center',
          justifyContent:'flex-end',
          borderBottomWidth:2,
          width:25,
          borderColor:borderColor,
        }
        return style;
      }
      return style;
    }

    reRander = () =>{
      const {reRander} = this.reRander;
      const rander = reRander ? false : true;
      this.setState({
        reRander: rander
      });
    }

    updateCalendar = (data) =>{
      const {calendar,nowDay} = this.state;
      if(calendar[data.year][data.month][data.key].type != 0){

          calendar[nowDay.year][nowDay.month][nowDay.key].type = 2;
          const newYear = data.year;
          const newMonth = data.month;
          const newDay = data.day;
          const newKey = data.key;

          const newDate = {
            year:newYear,
            month:newMonth,
            day:newDay,
            key:newKey,
          };
          const week = calendar[newYear][newMonth][newKey].row;
          calendar[newYear][newMonth][newKey].type = 1;
          this.setState({
            calendar:calendar,
            nowDay:newDate,
            week:week,
          });
        }
    }

    selectDay = (data) =>{
      if(data != undefined){
        this.state.dateCallback(data);
        this.updateCalendar(data);
      }else{console.log("undefined data");}
    }

    viewLevelChange = (level)=>{
      this.setState({
        viewLevel:level
      });
    }

    getCalendarView = () =>{
      const {viewLevel} = this.state;

      if(viewLevel == 3){
        return this.DayView();
      }else if(viewLevel == 2){
        return  this.MonthView();
      }else if(viewLevel == 1){
        return this.YearView();
      }
    }

    MarkerStyle = (day) =>{
      const {backgroundColor,accentColor,marker,calendarType} = this.state;
      const type = day.type;
      const mkStr = day.year+'/'+day.month+'/'+day.day;
      
      if(marker.includes(mkStr)){
        if(type>=1){
          return accentColor;
        }else{
          if(calendarType == 'Booking')return '#e2e2e2';
          else if(calendarType == 'Calendar') return accentColor;
          else return accentColor;
        }
      }else{
        return backgroundColor;
      }
    }
  

    DayView = () =>{
      const {calendar,changeCalendar,backgroundColor} = this.state;
      const Year = changeCalendar.year;
      const Month = changeCalendar.month;
      const days = this.getAllDay(calendar,Year,Month);
      const superViews=[];
        for(let i=0;i<6;i++){
          const childViews=[];
          for(let j=0;j<7;j++){
            let k = String(i+'-'+j);
            let value = '';
            let color = backgroundColor,borderStyle = {};
            let markerColor = backgroundColor;
            if(days[k] != undefined){
              value = days[k].day;
              color = this.getColor(days[k].type);
              borderStyle = this.DayStyle(days[k].type);
              markerColor = this.MarkerStyle(days[k]);
            }
            childViews.push(<TouchableOpacity key={j} style={styles.childView} 
            onPress={()=>this.selectDay(days[k])}>
            <View style={borderStyle}>
            <Text style={{color:markerColor,fontSize:15,fontWeight:'bold'}}>.</Text>
              <Text style={{color:color}}>{value}</Text>
            </View>
            </TouchableOpacity>);
          }
          superViews.push(<View key={i} style={styles.superView}>
          {childViews}</View>
          )
        }
        return superViews;
    }

    MonthStyle = (key) =>{
      const {defaultColor,accentColor,changeCalendar,backgroundColor,borderColor} = this.state;
      const month = changeCalendar.month;
      let txtColor = defaultColor;
      let style = {
        justifyContent:'center',
        alignItems:'center',
        width:50,
        height:50,
       
      }
      if(key == month){
        txtColor = backgroundColor;
        style = {
          justifyContent:'center',
          alignItems:'center',
          borderRadius:30,
          width:50,
          height:50,
          backgroundColor:accentColor,
        }
      }
      return <TouchableOpacity onPress={()=>this.selectMonth(key)} style={style}>
                <Text style={{color:txtColor, fontSize:18}}>{key}</Text>
              </TouchableOpacity>
    }

    selectMonth = (month) =>{
      const {changeCalendar} = this.state;
      changeCalendar.month = month;
      this.setState({
        changeCalendar:changeCalendar
      });
      this.viewLevelChange(3);
    }

    MonthView = () =>{
      const superViews=[];
      let monthCount = 1;
      for(let i=0;i<3;i++){
        const childViews=[];
        for(let j=0;j<4;j++){
          const month = monthCount;
          childViews.push(
            <View key={j} style={styles.childView}>
              {this.MonthStyle(month)}
            </View>
          )
          monthCount += 1;
        }
        superViews.push(<View key={i} style={styles.superView}>{childViews}</View>)
      }
      return superViews;
    }

    selectYear= (year) =>{
      const {changeCalendar} = this.state;
      changeCalendar.year = year;
      changeCalendar.month = 1;
      this.setState({
        changeCalendar:changeCalendar
      });
      this.viewLevelChange(2);
    }

    YearStyle = (key) =>{
      const {defaultColor,changeCalendar,backgroundColor,borderColor} = this.state;
      const year = changeCalendar.year;
      let txtColor = defaultColor;
      let style = {
        justifyContent:'center',
        alignItems:'center',
        width:50,
        height:50,
      }
      if(key == year){
        txtColor = backgroundColor;
        style = {
          justifyContent:'center',
          alignItems:'center',
          borderRadius:30,
          width:50,
          height:50,
          backgroundColor:borderColor,
        }
      }
      return <TouchableOpacity onPress={()=>this.selectYear(key)} style={style}>
                <Text style={{color:txtColor, fontSize:18}}>{key}</Text>
              </TouchableOpacity>
    }


    YearView = () =>{
      const {calendar,year} = this.state;
    
      let i = 0;
      const superViews = [];
      let childViews = [];
      for(key in calendar){
        childViews.push(<View key={i} style={styles.childView}>
        {this.YearStyle(key)}</View>);
        if(!((i+1) % 4)){
          superViews.push(<View key={i} style={styles.superView}>{childViews}</View>);
          childViews = [];
        }
        i+=1;
      }
      for(let j=childViews.length;j<4;j++){
        childViews.push(<View key={i} style={styles.childView}><Text></Text></View>);
        i+=1;
      }
      superViews.push(<View key={i} style={styles.superView}>{childViews}</View>);
      let views;
      if(year.startYear + year.endYear > 23){
        views = <ScrollView style={{height:'100%'}}>{superViews}</ScrollView>;
      }else{
        views = superViews;
      }
      return views;
    }

    pastMonth = ()=>{
      const {year,changeCalendar,standardDay} = this.state;
      let month = changeCalendar.month;
      let start_y = year.startYear;
      let now_y = changeCalendar.year;

      if(!((standardDay.year - start_y) == now_y && month == 1)){
        if(month != 1){
          changeCalendar.month = Number(month) - 1;
          this.setState({
            changeCalendar:changeCalendar
          });
        }else{
          changeCalendar.year = Number(now_y) - 1;
          changeCalendar.month = 12;
          this.setState({
            changeCalendar:changeCalendar,
          });
        }
      }else{
        console.log('[Calendar][back-month] : ',"can't go back");
      }
    }

    futureMonth = ()=>{
      const {year,changeCalendar,standardDay} = this.state;
      let month = changeCalendar.month;
      let end_y = year.endYear;
      let now_y = changeCalendar.year;
      if(!((standardDay.year + end_y) == now_y && month == 12)){
        if(month != 12){
          changeCalendar.month = Number(month) + 1;
          this.setState({
            changeCalendar:changeCalendar
          });
        }else{
          changeCalendar.year = Number(now_y) + 1;
          changeCalendar.month = 1;
          this.setState({
            changeCalendar:changeCalendar,
          });
        }
      }else{
        console.log('[Calendar][back-month] : ',"can't go forward");
      }
    }

    headerView = () =>{
      const {changeCalendar,accentColor,defaultColor,defaultText,viewLevel} = this.state;
      return <View style={{width:'100%',paddingLeft:15,paddingRight:15,flexDirection:'row',flex:0.5}}>
      <TouchableOpacity style={[styles.headerViewTouch,{justifyContent:'flex-start'}]} onPress={()=>this.viewLevelChange(1)}>
          <Text style={[styles.headerText,{color:defaultColor}]}>{changeCalendar.year}</Text>
          <Text style={{color:defaultColor}}>{defaultText.year}</Text>
      </TouchableOpacity>
      <TouchableOpacity style={[styles.headerViewTouch,{justifyContent:'center'}]} onPress={()=>this.viewLevelChange(2)}>
          <Text style={[styles.headerText,{color:defaultColor}]}>{changeCalendar.month}</Text>
          <Text style={{color:defaultColor}}>{defaultText.month}</Text>
      </TouchableOpacity>
      <View style={{flex:1}}>
        {
          viewLevel == 3 ? (
            <View style={{flexDirection:'row',alignItems:'center',justifyContent:'center',flex:1}}>
              <View style={styles.UpDownView}>
                <TouchableOpacity onPress={()=>this.futureMonth()}><AntDesign name='up' color={accentColor} size={20}/></TouchableOpacity>
              </View>
              <View style={styles.UpDownView}>
                <TouchableOpacity onPress={()=>this.pastMonth()}><AntDesign name='down' color={accentColor} size={20}/></TouchableOpacity>
              </View>
            </View>
          ) : (
              null
          )
        }
        
      </View>
      </View>
    }

    render(){
        const {weekly,backgroundColor} = this.state;
        return (
            <View style={[styles.container,{backgroundColor:backgroundColor}]}>
                {this.headerView()}
                <View style={{width:'100%',flex:2}}>
                {this.getCalendarView()}
                </View>
                <View style={styles.weeklyStyle}>{this.getWeeklyView(weekly)}</View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      width:'100%',
      height:'100%',
    },
    weeklyStyle:{
      flexDirection: 'row',
      width:'100%',
      flex:0.5
    },
    weekView:{
      flex:1,
      alignItems:'center',
      justifyContent:'center'
    },
    selectBorder:{
      alignItems:'center',
      justifyContent:'center',
      borderRadius:30,
      borderWidth:1,
      width:30,
      height:30,
      borderColor:'#421'
    },
    childView:{
      flex:1,
      alignItems:'center',
      justifyContent:'center'
    },
    superView:{
      flexDirection:'row',
      width:'100%',
      flex:1
    },
    headerViewTouch :{
      flex:1, 
      flexDirection:'row',
      alignItems:'center'
    },
    headerText:{
      fontSize:18, 
      fontWeight:'300',
    },
    UpDownView:{
      alignItems:'flex-end',
      justifyContent:'center',
      flex:1
    }

});
  