import React from 'react';
import { StyleSheet, Text, View ,TouchableOpacity,Alert} from 'react-native';
import RK_Calendar from './RK_Calendar';
import RK_Scheduler from './RK_Scheduler';

const weekly = ['일','월','화','수','목','금','토'];

export default class App extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      pageFlag:false
    }
  }

  showCalendar = () =>{
    this.setState({
      pageFlag:true,
    })
  }

  dismissCalendar = () =>{
    this.setState({
      pageFlag:false,
    })
  }

  calendarCB = (data) =>{
    //this.event(data.type);
    console.log("[CalendarCB] : ",data);
    const d = new Date();
    let nowDay = (d.getFullYear()*360)+((d.getMonth()+1)*30)+d.getDate();
    let evtDay = (data.year*360)+(data.month*30)+data.day;
    if(evtDay>=nowDay){
      //const newDay = {year:data.year,month:data.month,day:data.day,key:data.key};
      const newDay = {year:data.year,month:data.month,day:data.day};
      this.setState({
        nowDay:newDay,
      });
    }
  }

  sameDayWeek = (data) =>{
    console.log("[SameDayWeek] : ",data);
  }

  

  componentWillMount(){
    const d = new Date();
    const nowDay = {year:d.getFullYear(),month:d.getMonth()+1,day:d.getDate()};
    const week = d.getDay();
    const BKInfo =[
      {date:{year:2019,month:5, day:23,},bookingTime:[{start:'17:00',end:'17:20'},{start:'20:20',end:'20:40'} ],msg:['인원 5명','인원 2명']},
      {date:{year:2019,month:5, day:28,},bookingTime:[{start:'22:00',end:'22:20'} ],msg:['VERO Coffe - 아이스 아메리카노 2잔, 자몽에이드 5잔, 카페라때 10잔 총 17잔 (후불 결제)']},
    ];
    const Markers = [
      '2019/4/2',
      '2019/5/23',
      '2019/5/28',
    ];

    this.setState({
      nowDay:nowDay,
      week:week,
      BKInfo:BKInfo,
      Marker:Markers,
    });
  }


  showStyle = (pageFlag) =>{
    if(pageFlag){
      return <Text style={{fontSize:20,color:'blue'}}>Show</Text>
    }else{
      return <Text style={{fontSize:15,color:'#000'}}>Show</Text>
    }
  }

  dismissStyle = (pageFlag) =>{
    if(!pageFlag){
      return <Text style={{fontSize:20,color:'blue'}}>Dismiss</Text>
    }else{
      return <Text style={{fontSize:15,color:'#000'}}>Dismiss</Text>
    }
  }

  bookingTime = (data) =>{
    console.log("[BookingTime] : ", data);
    this.bookingAlert(data);
  }

  bookingAlert = (data) =>{
    const state = data.state;
    if(state == 'not available'){
      Alert.alert(
        '예약하기',
        data.message,
        [
          {text: '확인', onPress: () => console.log('OK Pressed')},
        ],
        {cancelable: false},
      )
    }else if(state == 'holiday'){
      Alert.alert(
        '예약하기',
        data.message,
        [
          {text: '확인', onPress: () => console.log('OK Pressed')},
        ],
        {cancelable: false},
      )
    }else if(state == 'break time'){
      Alert.alert(
        '예약하기',
        data.message,
        [
          {text: '확인', onPress: () => console.log('OK Pressed')},
        ],
        {cancelable: false},
      )
    }else if(state == 'available'){
      let msg = data.date.year + '/' + data.date.month + '/' + data.date.day + '  ';
      msg += data.time.start +'\n';
      msg += '예약 하시겠습니까?' 
      Alert.alert(
        '예약하기',
        msg,
        [
          {text: '아니요', onPress: () => console.log('NO Pressed')},
          {text: '예', onPress: () => this.addBooking(data)}
        ],
        {cancelable: false},
      )
    }else if(state == 'reservation'){
      let msg = data.date.year + '/' + data.date.month + '/' + data.date.day + '  ';
      msg += data.time.start +'\n';
      msg += data.message + '\n';
      msg += '예약 취소 하시겠습니까?';
      Alert.alert(
        '예약취소',
        msg,
        [
          {text: '예', onPress: () => this.deletBooking(data)},
          {text: '아니요', onPress: () => console.log('NO Pressed')},
        ],
        {cancelable: false},
      )
    }
  }
 
  //예약 추가시키는 함수
  addBooking = (data) =>{
    const {BKInfo,Marker} = this.state;
    const ndate = data.date;

    const addMarker = ndate.year+'/'+ndate.month+'/'+ndate.day;
    if(!Marker.includes(addMarker)) Marker.push(addMarker);

    for(key in BKInfo){
      const bkDate = BKInfo[key].date;
      if(ndate.year == bkDate.year && ndate.month == bkDate.month && ndate.day == bkDate.day){
        BKInfo[key].bookingTime.push(data.time);
        BKInfo[key].msg.push('e-Mart 혜자 스러운 도시락 3,500원 2개');
        this.setState({
          BKInfo:BKInfo,
          Marker:Marker,
        });
        return;
      }
    }

    const newBKInfo = {
      date:{year:ndate.year,month:ndate.month,day:ndate.day},
      bookingTime:[data.time],
      msg:['칼로리스테이션 : ICE 아마리카노 2잔, 오일파스타 1개, 고로케 1개, 셀러드 파스타 1개 (결제완료) '],
    }

    BKInfo.push(newBKInfo);
    this.setState({
      BKInfo:BKInfo,
      Marker:Marker,
    });
  }

  //예약 삭제 하는함수
  deletBooking = (data) =>{
    const {BKInfo,Marker} = this.state;
    const bDate = data.date;
    const bTime = data.time;
    for(dKey in BKInfo){
      let info = BKInfo[dKey];
      if(info.date.year == bDate.year && 
        info.date.month == bDate.month && info.date.day == bDate.day){
          let time = info.bookingTime;
          let index = 0;
          for(tKey in time){
            let stime = time[tKey].start.split(':');
            let etime = time[tKey].end.split(':');

            let bStime = bTime.start.split(':');
            if(this.parserMS(bStime[0],bStime[1],0) >= this.parserMS(stime[0],stime[1],0) &&
               this.parserMS(bStime[0],bStime[1],0)<this.parserMS(etime[0],etime[1],0)){
                
               BKInfo[dKey].bookingTime.splice(index,1); 
            }
            index++;
          }
          if(time.length == 0){
            let key = info.date.year+'/'+info.date.month+'/'+info.date.day;
            Marker.splice(Marker.indexOf(key),1);
          }
      }
    }
    this.setState({
      BKInfo:BKInfo,
      Marker:Marker,
    });
  }

  parserMS = (hour,minute,second) =>{
    let h = (hour*60)*60;
    let m = minute*60;
    let s = second;
    return h+m+s;
  }

  render() {
    const {pageFlag,nowDay,BKInfo,Marker} = this.state;
    
    
    const d = new Date();
    const nowTime = {hours:d.getHours(),minutes:d.getMinutes(),seconds:d.getSeconds()};
    const message = {notAvailableMSG:'예약할 수 없습니다.',availableMSG:'예약이 가능합니다.',breakTimeMSG:'쉬는시간 입니다.',holidayMSG:'휴일 입니다.'};

    return (
      <View style={styles.container}>
        <View style={styles.mainContainer}>
          {
            pageFlag == false ?(
              <View>
                <Text>
                  Hello Scheduler
                </Text>
              </View>
            ) : (
              <View style={{marginTop:20,width:'100%',height:'100%'}}>
              <View style={{flex:1,width:'100%'}}>
                <RK_Calendar
                  year={{startYear:5,endYear:5}}
                  viewType={'bookingView'}
                  dateCallback={(data)=>{this.calendarCB(data)}}
                  backgroundColor = {'#fff'}
                  defaultColor = {'#000'}
                  accentColor = {'#ec654b'}
                  borderColor = {'#4b9a4a'}
                  weekly={weekly}
                  calendarType = {'Booking'}
                  defaultText={{year:'년',month:'월',day:'일'}}
                  nowDay ={nowDay}
                  style={{flex:1}}
                  marker={Marker}
                />
              </View>
              <View style={{flex:1,width:'100%'}}>
                <RK_Scheduler
                  backgroundColor = {'#fff'}
                  defaultColor = {'#000'}
                  accentColor = {'#ec654b'}
                  selectEvt={(data)=>{this.bookingTime(data)}}
                  time={{minTime:'08:00',maxTime:'24:00',unit:20,breakTime:[{start:'11:00', end:'13:00'}],breakDay:[0,6]}}
                  nowDate={nowDay}
                  nowTime={nowTime}
                  booking={BKInfo}
                  style={{flex:1}}
                  id={'Rokku'}
                  message={message}
                />
              </View>
              </View>
            )
          }
        </View>
        <View style={styles.buttonContainer}>
          <TouchableOpacity onPress={()=>this.dismissCalendar()} style={styles.buttonView}>
            {this.dismissStyle(pageFlag)}
          </TouchableOpacity>
          <TouchableOpacity onPress={()=>this.showCalendar()} style={styles.buttonView}>
            {this.showStyle(pageFlag)}
          </TouchableOpacity>
        </View>
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  mainContainer:{
    flex:3,
    width:'100%',
    backgroundColor:'#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonContainer:{
    flex: 0.5,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  buttonView:{
    flex:0.5,
    alignItems: 'center',
    justifyContent: 'center',
  }
});
