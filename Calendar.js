import React from 'react';
import { StyleSheet,View,Text,TouchableOpacity,Alert} from 'react-native';
import {AntDesign} from '@expo/vector-icons';

function youndal(year){
  if(year%4==0 && year%100!=0 || year%400==0)
    return 1;
  else 
    return 0;
  
}

function totalDay(year,month){
  if(month==1 || month==3 || month==5 || month==7 || month==8 || month==10 || month==12)
    return 31;
  else if(month==4 || month==6 || month==9)
    return 30;
  else 
    return 28+youndal(year);
  
}

//start 
//end
// 이번해를 기준을 start~2019~end 날짜 설정
// week[] 월,화,수... 표현 
export function createDate(Year,weekly,nowDay){
  const startYear = Year.startYear;
  const endYear = Year.endYear;
  const nowYear = nowDay.year;
  const CALENDER = {};
  for(let year = (nowYear-startYear); year<= (nowYear+endYear);year++){
    
    const MONTH = {};
    for(let month = 0; month < 12; month++){
      
      var days = totalDay(year,month+1);
      var jucha = 1;
      const DAYS = {};
      for(let day=1;day<=days;day++){
       

        //과거 / 현재 / 미래 를 나눠야 한다
        var type = timeType(nowDay,year,month+1,day);
        var a = new Date(year,month,day);
        var d = a.getDay();
        var w = weekly[d];
        var key = String(jucha-1+'-'+d);
        const date = {'row':d,'column':jucha-1,'day':day,'week':jucha,'weekly':w,'type':type,'year':year,'month':month+1,'key':key};
        DAYS[key] = date;
        if(d==6) jucha +=1;
      }
      
      MONTH[String(month+1)]=DAYS;
    }
    
    CALENDER[String(year)]=MONTH;
  }
  return CALENDER;
}

export function timeType(nowDay,year,month,day){
    let dd = nowDay.day;
    let mm = nowDay.month;
    let yy = nowDay.year;

    const a= (year*365)+(month*30)+day;
    const b= (yy*365)+(mm*30)+dd;
    if(a == b) return 1;
    else if(a > b) return 2;
    else return 0;

}

export default class Calendar_Guava extends React.Component{

    constructor(props){
        super(props);
        /*
            Year = {startYear:1,endYear:5} 1년전 날짜 부터 5년후 날짜 까지
            Time = {minTime:'08:00' ,maxTime:'20:00' ,breakTime:[{start:'11:00',end:'13:00'},{start:'18:30',end:'19:00'}] ,unit:'5'}
            viewType = 'calendarView' / 'bookingView'
            callback = eventFunction
            defaultColor = defaultTextColor
            accentColor = change TextColor
            nowDay = {year:2019,month:5,day3}
            
        */
        this.state = {
            year:this.props.year,
            time:this.props.time,
            backgroundColor:this.props.backgroundColor,
            defaultColor:this.props.defaultColor,
            accentColor:this.props.accentColor,
            borderColor:this.props.borderColor,
            viewType:this.props.viewType,
            dateCallback:this.props.dateCallback,
            weekCallback:this.props.weekCallback,
            weekly:this.props.weekly,
            nowDay:this.props.nowDay,
            reRander:false,
            defaultText:this.props.defaultText,
            viewLevel:3,
        }
    }

    componentWillMount(){
        const {year,weekly,nowDay} = this.state;
        const changeCalendar = {year:nowDay.year,month:nowDay.month};
        const standardDay = {year:nowDay.year,month:nowDay.month,day:nowDay.day};
        const calendar = createDate(year,weekly,nowDay);
        const cal = this.getAllDay(calendar,nowDay.year,nowDay.month);
        
        for(k in cal){
          if(cal[k].type == 1) {
            Object.assign(nowDay,{key:k});
            break;
          }
        }
        const week = calendar[nowDay.year][nowDay.month][nowDay.key].row;
        this.setState({
          calendar:calendar,
          nowDay:nowDay,
          changeCalendar: changeCalendar,
          standardDay: standardDay,
          week: week,
        });
    }

    getWeeklyStyle = (key) =>{
      const {week,accentColor,defaultColor,borderColor} = this.state;
      if(week == key){
        return {
          fontSize:20,
          color:borderColor,
        }
      }else{
        return {
          fontSize:15,
          color:defaultColor,
        }
      }
    }

    getWeekValue = (key) =>{
      const {changeCalendar,calendar,weekCallback} = this.state;
      const year = changeCalendar.year;
      const month = changeCalendar.month;
      let sameDayOfTheWeek = [];
      const selectWeek = calendar[year][month];
      for(k in selectWeek){
        if(selectWeek[k].row == key){
          sameDayOfTheWeek.push(selectWeek[k]);
        }
      }
      weekCallback(sameDayOfTheWeek);
    }

    getWeeklyView = (weekly) =>{
      let weekview = [];
      for(key in weekly){
        const k = key;
        weekview.push(<TouchableOpacity key={key} style={styles.weekView} onPress={()=>this.getWeekValue(k)}><Text style={this.getWeeklyStyle(key)}>{weekly[key]}</Text></TouchableOpacity>)
      }
      return weekview;
    }

    getAllDay = (calendar,Year,Month) =>{
      if(calendar[Year] != undefined){
        if(calendar[Year][Month] != undefined){
          return calendar[Year][Month];
        }else console.log('[Calendar][MONTH]','데이터(MONTH) 입력 형식이 잘못 되었다.'); 
      }else console.log('[Calendar][YEAR]','데이터(YEAR) 입력 형식이 잘못 되었다.');
    }

    getColor = (type) =>{
      const {accentColor,defaultColor} = this.state;
      if(type == 0){
        return '#e2e2e2';
      }else if(type == 1){
        return accentColor;
      }else if(type == 2){ 
        return defaultColor;
      }
      return '#fff';
    }

    getBorderStyle = (type) =>{
      const {borderColor,backgroundColor} = this.state;
      let style = {
        alignItems:'center',
        justifyContent:'center',
        borderRadius:30,
        borderWidth:1,
        width:30,
        height:30,
        borderColor:backgroundColor
      }; 
      if(type == 1){
        style = {
          alignItems:'center',
          justifyContent:'center',
          borderRadius:30,
          borderWidth:1,
          width:30,
          height:30,
          borderColor:borderColor,
        }
        return style;
      }
      return style;
    }

    reRander = () =>{
      const {reRander} = this.reRander;
      const rander = reRander ? false : true;
      this.setState({
        reRander: rander
      });
    }

    updateCalendar = (data) =>{
      const {calendar,nowDay} = this.state;
      if(calendar[data.year][data.month][data.key].type != 0){

          calendar[nowDay.year][nowDay.month][nowDay.key].type = 2;
          const newYear = data.year;
          const newMonth = data.month;
          const newDay = data.day;
          const newKey = data.key;

          const newDate = {
            year:newYear,
            month:newMonth,
            day:newDay,
            key:newKey,
          };
          const week = calendar[newYear][newMonth][newKey].row;
          calendar[newYear][newMonth][newKey].type = 1;
          this.setState({
            calendar:calendar,
            nowDay:newDate,
            week:week,
          });
        }
    }

    selectDay = (data) =>{
      if(data != undefined){
        this.state.dateCallback(data);
        this.updateCalendar(data);
      }else{console.log("undefined data");}
    }

    viewLevelChange = (level)=>{
      this.setState({
        viewLevel:level
      });
    }

    getCalendarView = () =>{
      const {viewLevel} = this.state;

      if(viewLevel == 3){
        return this.DayView();
      }else if(viewLevel == 2){
        return  this.MonthView();
      }else if(viewLevel == 1){
        return this.YearView();
      }
    }

  

    DayView = () =>{
      const {calendar,changeCalendar} = this.state;
      const Year = changeCalendar.year;
      const Month = changeCalendar.month;
      const days = this.getAllDay(calendar,Year,Month);

      const superViews=[];
        for(let i=0;i<5;i++){
          const childViews=[];
          for(let j=0;j<7;j++){
            let k = String(i+'-'+j);
            let value = '';
            let color = '#fff',borderStyle = {};
            if(days[k] != undefined){
              value = days[k].day;
              color = this.getColor(days[k].type);
              borderStyle = this.getBorderStyle(days[k].type);
            }
            childViews.push(<TouchableOpacity key={j} style={{flex:1,justifyContent:'center',alignItems:'center'}} 
            onPress={()=>this.selectDay(days[k])}><View style={borderStyle}><Text style={{color:color}}>{value}</Text></View></TouchableOpacity>);
          }
          superViews.push(<View key={i} style={{flexDirection:'row',width:'100%',marginTop:10}}>
          {childViews}</View>
          )
        }
        return superViews;
    }

    MonthStyle = (key) =>{
      const {defaultColor,accentColor,changeCalendar,backgroundColor,borderColor} = this.state;
      const month = changeCalendar.month;
      let txtColor = defaultColor;
      let style = {
        justifyContent:'center',
        alignItems:'center',
        borderRadius:30,
        width:50,
        height:50,
        borderColor:backgroundColor,
        backgroundColor:backgroundColor,
      }
      if(key == month){
        txtColor = backgroundColor;
        style = {
          justifyContent:'center',
          alignItems:'center',
          borderRadius:30,
          width:50,
          height:50,
          backgroundColor:accentColor,
        }
      }
      return <TouchableOpacity onPress={()=>this.selectMonth(key)} style={style}>
                <Text style={{color:txtColor, fontSize:18}}>{key}</Text>
              </TouchableOpacity>
    }

    selectMonth = (month) =>{
      const {changeCalendar} = this.state;
      changeCalendar.month = month;
      this.setState({
        changeCalendar:changeCalendar
      });
      this.viewLevelChange(3);
    }

    MonthView = () =>{
      const superViews=[];
      let monthCount = 1;
      for(let i=0;i<3;i++){
        const childViews=[];
        for(let j=0;j<4;j++){
          const month = monthCount;
          childViews.push(
            <View key={j} style={{flex:1,justifyContent:'center',alignItems:'center'}}>
              {this.MonthStyle(month)}
            </View>
          )
          monthCount += 1;
        }
        superViews.push(<View key={i} style={{flexDirection:'row',width:'100%',height:'15%'}}>{childViews}</View>)
      }
      return superViews;
    }

    YearView = () =>{
      const {calendar,changeCalendar,year,standardDay} = this.state;
      const startYear = year.startYear;
      const endYear = year.endYear;
      let superCount = 0;
      let childCount = 0;
      const start = standardDay.year - year.startYear;
      const end = standardDay.year + year.endYear;

      const superViews = [];
      let childViews = [];
      for(start; start<end;start++){
        if(start>0 && !(start % 4)){
          superViews.push(<View key={superCount}></View>);
          childViews.remove();
          superCount += 1;
        }else{
          childViews.push(<View key={childCount}></View>);
          childCount += 1;
        }
      }

      return superViews;
    }

    pastMonth = ()=>{
      const {year,changeCalendar,standardDay} = this.state;
      let month = changeCalendar.month;
      let start_y = year.startYear;
      let now_y = changeCalendar.year;

      if(!((standardDay.year - start_y) == now_y && month == 1)){
        if(month != 1){
          changeCalendar.month = month - 1;
          this.setState({
            changeCalendar:changeCalendar
          });
        }else{
          changeCalendar.year = now_y - 1;
          changeCalendar.month = 12;
          this.setState({
            changeCalendar:changeCalendar,
          });
        }
      }else{
        console.log('[Calendar][back-month] : ',"can't go back");
      }
    }

    futureMonth = ()=>{
      const {year,changeCalendar,standardDay} = this.state;
      let month = changeCalendar.month;
      let end_y = year.endYear;
      let now_y = changeCalendar.year;
      
      if(!((standardDay.year + end_y) == now_y && month == 12)){
        if(month != 12){
          changeCalendar.month = month + 1;
          this.setState({
            changeCalendar:changeCalendar
          });
        }else{
          changeCalendar.year = now_y + 1;
          changeCalendar.month = 1;
          this.setState({
            changeCalendar:changeCalendar,
          });
        }
      }else{
        console.log('[Calendar][back-month] : ',"can't go forward");
      }
    }

    haderView = () =>{
      const {changeCalendar,accentColor,defaultColor,defaultText,viewLevel} = this.state;
      return <View style={{width:'100%',padding:15,flexDirection:'row'}}>
      <TouchableOpacity style={{flex:1, flexDirection:'row',justifyContent:'flex-start',alignItems:'center'}} onPress={()=>this.viewLevelChange(1)}>
          <Text style={{fontSize:18, fontWeight:'300',color:defaultColor}}>{changeCalendar.year}</Text>
          <Text style={{color:defaultColor}}>{defaultText.year}</Text>
      </TouchableOpacity>
      <TouchableOpacity style={{flex:1, flexDirection:'row',justifyContent:'center',alignItems:'center'}} onPress={()=>this.viewLevelChange(2)}>
          <Text style={{fontSize:18, fontWeight:'300',color:defaultColor}}>{changeCalendar.month}</Text>
          <Text style={{color:defaultColor}}>{defaultText.month}</Text>
      </TouchableOpacity>
      <View style={{flex:1}}>
        {
          viewLevel == 3 ? (
            <View style={{flexDirection:'row',alignItems:'center',justifyContent:'center',flex:1}}>
              <TouchableOpacity style={{alignItems:'flex-end',justifyContent:'center',flex:1}} onPress={()=>this.futureMonth()}>
                <AntDesign name='up' color={accentColor} size={20}/>
              </TouchableOpacity>
              <TouchableOpacity style={{alignItems:'flex-end',justifyContent:'center',flex:1}} onPress={()=>this.pastMonth()}>
                  <AntDesign name='down' color={accentColor} size={20}/>
              </TouchableOpacity>
            </View>
          ) : (
              null
          )
        }
        
      </View>
      </View>
    }

    render(){
        const {weekly,backgroundColor} = this.state;
        
        return (
            <View style={[styles.container,{backgroundColor:backgroundColor}]}>
                {this.haderView()}
                {this.getCalendarView()}
                <View style={styles.weeklyStyle}>{this.getWeeklyView(weekly)}</View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      width:'100%',
      height:'100%',
    },
    weeklyStyle:{
      flexDirection: 'row',
      width:'100%',
      marginTop:20,
    },
    weekView:{
      flex:1,
      alignItems:'center',
      justifyContent:'center'
    },
    selectBorder:{
      alignItems:'center',
      justifyContent:'center',
      borderRadius:30,
      borderWidth:1,
      width:30,
      height:30,
      borderColor:'#421'
    }

});
  