**RK_Calendar**

Parms	            Input Type				                                            설명
year        	    Object      startYear: Number	        endYear: Number		        staryYear ~ 현재 ~ endYear 까지 날짜 생성
dateCallback      	function	Function(data)	            callbackFunction(data)		날짜 클릭했을때 해당 날짜의 데이터가 return 된다.
backgroundColor  	String	    '#Color'			                                     Calendar View의 background Color
defaultColor  	    String	    '#Color'			                                    년/월/날짜 텍스트 기본 날짜 생성
accentColor  	    String	    '#Color'			                                    해당 날짜/ up down 버튼 / marker등 색깔
borderColor 	    String	    '#Color'			                                    해당 날짜 밑줄/ weekly text 색깔
weekly 	            Array	    ['일','월','화','수'...]	ArraySize = 7		        일~토 까지의 Text
defaultText	        Object	    year: String	            month: String	day: String	년/월/일 Text
nowDay	            Object	    year: Number	            month: Number	day:Number	현재 날짜 혹은 특정 날짜 지정
marker	            Array	    Array Type: String			                            표시가 되어야하는 날짜 
calendarType	    String	    Booking or Calendar			                            Calendar : 일반 달력 / Booking : 예약 달력



**Open Method**

Method Name	Params	                                                return	    설명
createDate	year(Object),weekly(Array),nowDay(Object)  	            Object      {2018:
            			                                                            {1:{
            			                                                             0-1:{row,column,day,week,weekly,type,year,month:,key},……..},
            			                                                             2:{}
            			                                                             …
            			                                                             12:{}
            			                                                             }
                                                                                }
timeType	nowDay(Object),year(Number),month(Number),day(Number)	Number	    과거:0, 현재:1, 미래:2

**Example**

<RK_Calendar
year={{startYear:5,endYear:5}}
dateCallback={(data)=>{this.calendarCB(data)}}
backgroundColor = {'#fff'}
defaultColor = {'#000'}
accentColor = {'#ec654b'}
borderColor = {'#4b9a4a'}
weekly={['일','월','화','수','목','금','토']}
defaultText={{year:'년',month:'월',day:'일'}}
nowDay ={{year:2019,month:5,day:24}}
marker={['2019/4/2','2019/5/23','2019/5/28']}
/>


**RK_Scheduler**

Parms	        Input Type						                                                                                                            설명

backgoundColor	String	        '#Color'					                                                                                                Scheduler View Background Color
defaultColor	String	        '#Color'					                                                                                                Text default Color
accentColor	    String	        '#Color'					                                                                                                예약이 있는 icon Color
selectEvt	    Function(data)	callbackFunction(data)					                                                                                    List item clieck시에 발생 하는 이벤트 해당 아이템의 정보를 return
time	        Object	        minTime: String             maxTime: String	        unit: Number	        breakTime:Array	        breakDay:Array	        시작/끝 시간 까지 설정, 시간 간격, 쉬는시간, 쉬는 날짜 설정
nowDate	        Object	        year: Number	            month: Number	        day: Number 			                                                현재/ 특정 날짜 설정
nowTime	        Object	        hours: Number	            minutes: Number	        seconds: Number			                                                현재 시간 설정 
booking	        Array	        Object					                                                                                                    예약 정보가 담겨있는 객체
id	            String/Number…						                                                                                                        이벤트 발생시 리턴 받을 고유 아이디
message	        Object	        notAvailableMSG: String	    availableMSG: String    breakTimeMSG: String	holidayMSG: String		                        예약불가/ 예약가능/ 쉬는시간/ 쉬는날짜를 나타내는 문구

**부가 정보**
**time**						                                    설명
breakTime:[	Object{	start: String	end: String	},…	]	            start: 쉬는시간 시작, end: 쉬는시간 끝(몇시 부터 몇시까지)  
breakDay:[	0~6	]				                                    0(일요일)을 기준으로~6(토요일) 쉬는 날짜에 맞는 값을 기입 

**booking	Type**					                                설명
date:	Object{	year: Number	month: Number	day: Number }	,	예약했던 날짜
bookingTime:	Array[	Object{	start: String	end: String },	…]	start: 예약시간 시작, end: 예약시간 끝(몇시 부터 몇시까지)  
Msg:	Array[	String,String	]			                        예약 문구

**Open Method**

MethodName	Parms	                                    Return	                        설명
parserMS	hour(Number),minute(Number),second(Number)	Ms(Number)	                    시,분,초 => 해당 미리세컨드 return
parserHMS	Ms(Number)	                                Object{hours,minutes,seconds}	미리세턴드 => 시,분,초 return

**Example**

<RK_Scheduler
backgroundColor = {'#fff'}
defaultColor = {'#000'}
accentColor = {'#ec654b'}
selectEvt={(data)=>{this.bookingTime(data)}}
time={{minTime:'08:00',maxTime:'24:00',unit:20,breakTime:[{start:'11:00', end:'13:00'}],breakDay:[0,6]}}
nowDate={{year:year,month:month,day:day}}
nowTime={{hours:Hours,minutes:Minutes,seconds:Seconds}}
booking={[{date:{year:2019,month:5, day:23,},bookingTime:[{start:'17:00',end:'17:20'},{start:'20:20',end:'20:40'} ],msg:['인원 5명','인원 2명']}],..}
id={'Rokku'}
message={{notAvailableMSG:'예약할 수 없습니다.',availableMSG:'예약이 가능합니다.',breakTimeMSG:'쉬는시간 입니다.',holidayMSG:'휴일 입니다.'}}
/>



