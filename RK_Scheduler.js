import React from 'react';
import { StyleSheet,View,Text,TouchableOpacity,Alert,ScrollView} from 'react-native';
import {AntDesign} from '@expo/vector-icons';

const TIME = {};

function setTimeEvnt(fun){
    if(TIME.TimeEvt == undefined){
        TIME.TimeEvt = fun;
    }
}

function deleteMemory(){
    TIME.TimeEvt = undefined;
    TIME.nowTime = undefined;
}

function startGetTime(){
    TIME.nowTime = setInterval(function(){
        console.log("New GetTime!");
        TIME.TimeEvt();
    },30000);
}
function stopGetTime(){
    console.log("stop GetTime!");
    clearInterval(TIME.nowTime);
    deleteMemory();
}


function getWeek(year, month, day){
    const a = new Date(year, month, day);
    return a.getDay();
}

export function parserMS(hour,minute,second){
    let h = (hour*60)*60;
    let m = minute*60;
    let s = second;
    return h+m+s;
}

export function pasrerHMS(ms){
    let hour = ms/3600;
    let minute = (ms/60)%60;
    let seconde = (ms%60)%60;

    let h = Math.floor(hour);
    let m = Math.floor(minute);
    let s = Math.floor(seconde);

    if(h>24) h = h-24;

    if(h<10) h = '0'+String(h);
    if(m<10) m = '0'+String(m);
    if(s<10) s = '0'+String(s);
    return {hours:h,minutes:m,seconds:s};
}

function parserM(minute){
    return minute*60;
}

export default class RK_Scheduler extends React.Component{

    constructor(props){
        super(props);
        this.state={
            time:this.props.time,
            accentColor:this.props.accentColor,
            defaultColor:this.props.defaultColor,
            backgroundColor:this.props.backgroundColor,
            nowDate:this.props.nowDate,
            nowTime:this.props.nowTime,
            booking:this.props.booking,
            selectEvt:this.props.selectEvt,
            message:this.props.message,
            id:this.props.id,
        };
    }
    componentWillMount(){
        const {time} = this.state;
        let minTime = time.minTime.split(':');
        let maxTime = time.maxTime.split(':');
        setTimeEvnt(this.updateTime);
        const changeSchedule = {
            hours:time.hours,
            minutes:time.minTimes,
            seconds:time.seconds,
        };
        if(maxTime[0] < minTime[0]){
            maxTime[0] = Number(maxTime[0])+24;
            time.maxTime = maxTime[0]+':'+maxTime[1];
            this.setState({
                time:time,
                changeSchedule:changeSchedule,
            })
        }
    }

    componentDidMount(){
        startGetTime();
    }

    componentWillUnmount(){
        stopGetTime();
    }
    componentWillReceiveProps(np){
        const {nowDate,booking} = this.state;
        const nD = np.nowDate;
        const oD = nowDate;
        
        if(!(nD.year == oD.year && nD.month == oD.month && nD.day == oD.day)){
            this.setState({
                nowDate:np.nowDate
            });
        }
        const nB = np.booking;
        if(booking.length != nB.length){
            this.setState({
                booking:nB
            });
        }else{
            for(key in nB){
                let nlength = nB[key].bookingTime.length;
                let olength = booking[key].bookingTime.length;
                if(nlength != olength){
                    this.setState({
                        booking:nB
                    });
                    return;
                }
            }
        }
    }

    updateTime = () =>{
        const {nowTime} = this.state; 
        const date = new Date();
        const h = date.getHours();
        const m = date.getMinutes();
        const s = date.getSeconds();
        nowTime.hours = h;
        nowTime.minutes = m;
        nowTime.seconds = s;
        this.setState({
            nowTime:nowTime
        });

    }

    bookingEvent = (msg, start ,state) =>{
        const {selectEvt,nowDate,time,id} = this.state;
        const unit = time.unit;
        const sTime = pasrerHMS(start);
        const eTime = pasrerHMS(start+parserM(unit));

        const data = {
            message:msg,
            time:{start:sTime.hours+':'+sTime.minutes,end:eTime.hours+':'+eTime.minutes},
            state:state,
            date:nowDate,
            id:id,
        }
        selectEvt(data);
    }

    schedulerItemView = (msg,type,start,state) =>{
        const {defaultColor,accentColor} = this.state;
        let color='#e2e2e2',iconName='close',iconColor=defaultColor;
        if(type == 0){
            color='#e2e2e2';
            iconName='close';
            iconColor='#e2e2e2'
        }else if(type == 1){
            color=defaultColor;
            iconName='checkcircleo';
            iconColor=accentColor;
        }else{
            color=defaultColor;
            iconName='check'
        }
        return <TouchableOpacity style={styles.schedulerTouchable} onPress={()=>this.bookingEvent(msg,start,state)}>
            <View style={styles.schedulerView}>
            <Text style={[{color:color},styles.textSize]}>{msg}</Text>
           </View>
            <View style={styles.schedulerIconView}>
                   <AntDesign name={iconName} size={20} color={iconColor}/>
            </View>
            </TouchableOpacity> 
    }

    

    childSchedulView = (start) =>{
        const {message,time,nowDate,nowTime,booking} = this.state;
        
        let dateType = this.comparisonDate();
        if(dateType >= 1){
         const breakDay = time.breakDay;
         const nowWeek = getWeek(nowDate.year, (nowDate.month-1), nowDate.day);
         for(key in breakDay){
             let bDay = breakDay[key];
            if(nowWeek == bDay){
                return this.schedulerItemView(message.holidayMSG,0,start,'holiday');
            }
         }
         
         const nMS = parserMS(nowTime.hours,nowTime.minutes,nowTime.seconds);
         if(dateType==1){
             if(start < nMS){
                return this.schedulerItemView(message.notAvailableMSG,0,start,'notavailable');
             }
         }
        
         const breakTime = time.breakTime;
         for(key in breakTime){
            let startTime = breakTime[key].start.split(':');
            let endTime = breakTime[key].end.split(':');
            let sMS = parserMS(startTime[0],startTime[1],0);
            let eMS = parserMS(endTime[0],endTime[1],0);
           
            if(start >= sMS && start < eMS){
                return this.schedulerItemView(message.breakTimeMSG,0,start,'break time');
            }
         }

         const bTime = booking;
         for(key in bTime){
             let bD = bTime[key];
             if(this._comparisonDate(bD.date)){
                 let bT = bD.bookingTime;
                 for(t_key in bT){
                    let startTime = bT[t_key].start.split(':');
                    let endTime = bT[t_key].end.split(':');
                    let sMS = parserMS(startTime[0],startTime[1],0);
                    let eMS = parserMS(endTime[0],endTime[1],0);

                    if(start >= sMS && start < eMS){
                        return this.schedulerItemView(bD.msg[t_key],1,start,'reservation');
                    }
                 }
             }
         }
         return this.schedulerItemView(message.availableMSG,2,start,'available');
        }else{
        return this.schedulerItemView(message.notAvailableMSG,0,start,'not available');
        }
    }

    comparisonDate = ()=>{
        const {nowDate} = this.state;
        const oDate = nowDate;
        const eDate = new Date();
        const sumODate = (oDate.year*360) + (oDate.month*30) + oDate.day;
        const sumEDate = (eDate.getFullYear()*360) + ((eDate.getMonth()+1)*30) + eDate.getDate();
        if(sumODate == sumEDate) return 1;
        else if(sumODate > sumEDate) return 2;
        else return 0;
    }

    _comparisonDate = (date)=>{
        const {nowDate} = this.state;
        const oDate = nowDate;
        //console.log('_ComparisonDate : ',date);
        const eDate = date;
        const sumODate = (oDate.year*360) + (oDate.month*30) + oDate.day;
        const sumEDate = (eDate.year*360) + (eDate.month*30) + eDate.day;
        if(sumODate == sumEDate) return 1;
        else return 0;
    }

    timeView = (start) =>{
        const {nowTime,defaultColor} = this.state;
        let txtColor = '#e2e2e2';
        const nMS = parserMS(nowTime.hours,nowTime.minutes,nowTime.seconds);
        const time = pasrerHMS(start);
        let dateType = this.comparisonDate();
        //같은 날짜 일때
        if(dateType == 1){
            if(start>=nMS) {txtColor = defaultColor;}
        }
        else if(dateType == 2) {txtColor = defaultColor;}
        else {txtColor = '#e2e2e2';}
        return <View key={start} style={styles.timeView}>
            <Text style={[{color:txtColor},styles.textSize]}>{time.hours+':'+time.minutes}</Text>
            </View>
    }

    SchedulerView = () =>{
        const {time,backgroundColor} = this.state;
        const minTime = time.minTime.split(':');
        const maxTime = time.maxTime.split(':');
        let start = parserMS(minTime[0],minTime[1],0);
        let end = parserMS(maxTime[0],maxTime[1],0);
        const unit = parserM(time.unit);
        const superView = [];
        for(start;start<=end;start+=unit){
            superView.push(
               <View key={start} style={{flexDirection:'row',width:'100%',alignItems:'center',justifyContent:'center'}}>
               {this.timeView(start)}
               {this.childSchedulView(start)}
               </View>                 
            )
        }

        return <ScrollView style={{height:'100%',width:'100%',backgroundColor:backgroundColor}}>{superView}</ScrollView>;
    }

    render(){
        const {time,nowDate} = this.state;
        
        return(
            <View style={styles.container}>{this.SchedulerView()}</View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      width:'100%',
      height:'100%',
    },
    timeView:{
        justifyContent:'center',
        alignItems:'center',
        borderRightWidth:1,
        borderColor:'#e2e2e2',
        flex:1,
        height:80
    },
    textSize:{
        fontSize:15
    },
    schedulerTouchable:{
        flex:3,
        alignItems:'center',
        justifyContent:'center',
        height:80,
        flexDirection:'row'
    },
    schedulerView:{
        flex:2,
        justifyContent:'center',
        paddingLeft:10
    },
    schedulerIconView:{
        flex:0.5,
        alignItems:'center',
        justifyContent:'center'
    }
});